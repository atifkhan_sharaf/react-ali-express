import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  StatusBar,
  SafeAreaView
} from 'react-native';
import { WebBrowser } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { SearchBarComponent, DealGalleryComponent, CategoryDealsGallery, HomeActionsComponent, ProductGalleryComponent } from '../components'

import { MonoText } from '../components/StyledText';

function AppStatusBar() {

  return <SafeAreaView style={{backgroundColor: '#fff'}}>
    <StatusBar
      backgroundColor="white"
      barStyle="dark-content"
    />
    </SafeAreaView>
}

export default class HomeScreen extends React.Component {
  static navigationOptions = (navigation) => {
    return {
      header: null,
    };
  }

  dealData() {
    return [{key:"1", img:"http://s.sdgcdn.com/7/2018/04/Mobile_Bose-1.png"}, 
      {key:"2", img:"http://s.sdgcdn.com/7/2018/04/S9S9Plus170418-Mobile_1000X333.jpg"}, 
      {key:"3", img:"http://s.sdgcdn.com/7/2018/04/God_of_war_web_banner_1000x333-2.png"}, 
      {key:"4", img:"http://s.sdgcdn.com/7/2018/04/AC_Exchange_Web_Banner_1000x333.png"},
      {key:"5", img:"http://s.sdgcdn.com/7/2018/04/website_1000x333_170418-01.jpg"},
      {key:"6", img:"http://s.sdgcdn.com/7/2018/04/surface_laptop_HP2130_Web_Banner_1000x333-2.png"},
      {key:"7", img:"http://s.sdgcdn.com/7/2018/04/S9-Trade-in-mobile.jpg"},
      {key:"8", img:"http://s.sdgcdn.com/7/2018/04/WEBSITE_IPAD_ARTWORKS_1000x333.jpg"}

    ]
  }

  featuredBrandsData() {
    return [{key:"1", img:"https://d1bdmzehmfm4xb.cloudfront.net/original/3X/0/4/045d1ff0de79a7412c5b7ab83f84e715b19cd4b6.jpg"}, 
    {key:"2", img:"https://dzvfs5sz5rprz.cloudfront.net/media/wysiwyg/iPhone-8-8plus-x-banner_1_.jpg"}, 
    {key:"3", img:"https://images.vexels.com/media/users/3/126443/raw/ff9af1e1edfa2c4a46c43b0c2040ce52-macbook-pro-touch-bar-banner.jpg"}, 
    {key:"4", img:"https://image.ebuyer.com/customer/promos/custom-page-assets/BD103-Acer/Header-Banner.jpg"}]
  }

  featuredDealsData() {
    return [{key:"1", img:"https://dzvfs5sz5rprz.cloudfront.net/media/wysiwyg/iPhone-8-8plus-x-banner_1_.jpg"}, 
    {key:"2", img:"https://image.ebuyer.com/customer/promos/custom-page-assets/BD103-Acer/Header-Banner.jpg"}, 
    {key:"3", img:"https://images.vexels.com/media/users/3/126443/raw/ff9af1e1edfa2c4a46c43b0c2040ce52-macbook-pro-touch-bar-banner.jpg"}, 
    {key:"4", img:"https://d1bdmzehmfm4xb.cloudfront.net/original/3X/0/4/045d1ff0de79a7412c5b7ab83f84e715b19cd4b6.jpg"}]
  }

  topSellingProductData() {

    const img1 = 'https://s.sdgcdn.com/7/2018/03/78d18821edd82fc5f5fdd791a88f7b7e67148834_Huawei_nova_3e_sharafDG_UAE-1-600x600.jpg'
    const img2 = 'https://s.sdgcdn.com/7/2018/02/4b365dbc66dacf22b04fa26b35d6e0014d612e1c_sharaf_dg_online_best_price_iphone_x-1-600x600.jpg'
    const img3 = 'https://s.sdgcdn.com/7/2017/11/940c02890dfd3ccde52841dcb84fc0952dcf7efa_SM_N950F_GalaxyNote8_Front_Pen_Orchidgrey_30002000-1-600x600.jpg'
    const img4 = 'https://s.sdgcdn.com/7/2017/11/c448d007c3cecc8b553925b99255d65528cf3d55_galaxy_s8_plus_gallery_front_orchidgray_s4-1.png'

    return [
      {key:"1", title:"Huawei Nova 3e 64GB Midnight Black 4G Dual Sim ", oldPrice: 1500, price:1100, imgLink:img1}, 
      {key:"2", title:"Apple iPhone X 256GB Space Grey With FaceTime", oldPrice: 4965, price:4479, imgLink:img2}, 
      {key:"3", title:"Samsung Galaxy Note8 4G 64GB Orchid Grey", oldPrice: 3569, price:2899, imgLink:img3}, 
      {key:"4", title:"Samsung Galaxy S8+ 4G Dual Sim Smartphone 64GB Orchid Grey", oldPrice: 3254, price:2349, imgLink:img4}, 
    ]
  }

  topSellingTVs() {

    const img1 = 'https://s.sdgcdn.com/7/2018/01/96d0d541c7d41618bcad0934b4cc8570b63563ea_75put6303_edit__002_-1-600x600.jpg'
    const img2 = 'https://s.sdgcdn.com/7/2018/01/67c82bdafe1d8b844d57c55f92fff512d6d979c8_1-1-600x600.jpg'
    const img3 = 'https://s.sdgcdn.com/7/2017/10/89a3375831bc2e51634a06038b078264d6041e3b_UJ63_A_65_60_55_49_medium001-1-600x600.jpg'
    const img4 = 'https://s.sdgcdn.com/7/2018/01/06248ce55907ac7018c6d8f847f49303c0ae246e_2-1-600x600.jpg'

    return [
      {key:"1", title:"Philips 75PUT6303 4K UHD Ultra Slim Smart LED Television 75inch", oldPrice: 6824, price:5999, imgLink:img1}, 
      {key:"2", title:"TCL 75C2000MUS 4K UHD Smart LED Television 75inch", oldPrice: 9449, price:8449, imgLink:img2}, 
      {key:"3", title:"LG 65UJ651V 4K Ultra HD Smart LED Television 65inch", oldPrice: 4724, price:4299, imgLink:img3}, 
      {key:"4", title:"Sony 65X7000E 4K UHD Smart LED Television 65inch", oldPrice: 5299, price:4699, imgLink:img4}, 
    ]
  }

  topSellingLaptops() {

    const img1 = 'https://s.sdgcdn.com/7/2018/03/9433c9f02cc12f698aa2a1b6b36147c1c2d38b6e_5_Platinum_Laptop_Front-1-600x600.jpg'
    const img2 = 'https://s.sdgcdn.com/7/2017/11/3046c9718332e88dd35ebfb92682084283538ba4_mbp13_gray_select_cto_201610_GEO_AE-3-600x600.jpg'
    const img3 = 'https://s.sdgcdn.com/7/2018/02/193316c40f4137f450c4d5264ed30b997707d48e_Aspire7_gallery_01-1.jpg'
    const img4 = 'https://s.sdgcdn.com/7/2018/03/9d095ab75f68746777d2f0c69eb787f9b06bacb0_71blXmClLvL._SL1500_-1-600x600.jpg'

    return [
      {key:"1", title:"Microsoft Surface Laptop – Core i5 2.5GHz 4GB 128GB Shared Win10s 13.5inch UHD Platinum", oldPrice: 4513, price:4403, imgLink:img1}, 
      {key:"2", title:"Apple MacBook Pro – Core i5 2.3GHz 8GB 128GB Shared 13.3inch Space Grey Arabic", oldPrice: 9449, price:8449, imgLink:img2}, 
      {key:"3", title:"Acer Aspire 7 Laptop – Core i7 2.8GHz 12GB 1TB 4GB Win10 15.6inch FHD Black", oldPrice: 4500, price:4094, imgLink:img3}, 
      {key:"4", title:"Microsoft Surface Pro – Core i5 2.60GHz 4GB 128GB Shared Win10Pro 12.3inch Silver", oldPrice: 5483, price:3674, imgLink:img4}, 
    ]
  }
  

  render() {
    
    const bannerData = [{key:""}, {key:""}, {key:""}, {key:""}]
    const { navigation } = this.props

    return (
      <View style={styles.container}> 
        <AppStatusBar />
        <ScrollView>
          <View style={[styles.componentGroup, {marginTop:0, padding:20}]}>
            <SearchBarComponent />
            <DealGalleryComponent style={{marginTop:10, height:165  }} datas={this.dealData()} imageKey="img"/>
            <HomeActionsComponent />
          </View>
          
          <View style={styles.componentGroup}>
            <CategoryDealsGallery title="Featured Brands" data={this.featuredBrandsData()}/>
          </View>

          <View style={styles.componentGroup}>
            <CategoryDealsGallery title="Flash Deals" subtitle="2 Days Left" data={this.featuredDealsData()} />
          </View>

          <View style={styles.componentGroup}>
            <ProductGalleryComponent title="Top Selling Smart Phones" data={this.topSellingProductData()} onSelect={(product)=>{
              navigation.navigate('Search')
            }}/>
          </View>

          <View style={styles.componentGroup}>
            <ProductGalleryComponent title="Top Selling Laptops" data={this.topSellingLaptops()} onSelect={(product)=>{
              navigation.navigate('Search')
            }}/>
          </View>

          <View style={styles.componentGroup}>
            <ProductGalleryComponent title="Top Selling TV's" data={this.topSellingTVs()}/>
          </View>

        </ScrollView>        
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(244,245,246)',
    justifyContent:'flex-start',
    alignItems:'stretch'
  },
  componentGroup:{
    marginTop:10, padding: 20, backgroundColor:'white'
  }
});
