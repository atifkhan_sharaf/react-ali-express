import React, {Component} from 'react';
import {
  View,
  FlatList,
  Dimensions
} from 'react-native';
import { ProductCellComponent } from '../components'


export default class SearchScreen extends Component {

    topSellingProductData() {

        const img1 = 'https://s.sdgcdn.com/7/2018/03/78d18821edd82fc5f5fdd791a88f7b7e67148834_Huawei_nova_3e_sharafDG_UAE-1-600x600.jpg'
        const img2 = 'https://s.sdgcdn.com/7/2018/02/4b365dbc66dacf22b04fa26b35d6e0014d612e1c_sharaf_dg_online_best_price_iphone_x-1-600x600.jpg'
        const img3 = 'https://s.sdgcdn.com/7/2017/11/940c02890dfd3ccde52841dcb84fc0952dcf7efa_SM_N950F_GalaxyNote8_Front_Pen_Orchidgrey_30002000-1-600x600.jpg'
        const img4 = 'https://s.sdgcdn.com/7/2017/11/c448d007c3cecc8b553925b99255d65528cf3d55_galaxy_s8_plus_gallery_front_orchidgray_s4-1.png'
    
        return [
          {key:"1", title:"Huawei Nova 3e 64GB Midnight Black 4G Dual Sim ", oldPrice: 1500, price:1100, imgLink:img1}, 
          {key:"2", title:"Apple iPhone X 256GB Space Grey With FaceTime", oldPrice: 4965, price:4479, imgLink:img2}, 
          {key:"3", title:"Samsung Galaxy Note8 4G 64GB Orchid Grey", oldPrice: 3569, price:2899, imgLink:img3}, 
          {key:"4", title:"Samsung Galaxy S8+ 4G Dual Sim Smartphone 64GB Orchid Grey", oldPrice: 3254, price:2349, imgLink:img4}, 
        ]
    }

   

    numberOfItemInRow() {
        const screenWidth = Dimensions.get('screen').width
        return Math.max(Math.floor(screenWidth / 150), 2)
    }

    cellWidth() {        
        const numberOfItemInRow = this.numberOfItemInRow()
        const screenWidth = Dimensions.get('screen').width
        return screenWidth/numberOfItemInRow
    }

    renderItem(item) {
        return <ProductCellComponent product={item} numColumns={this.numberOfItemInRow()} style={{width:this.cellWidth()}}/>
    }

    render() {

        return <FlatList
            data={this.topSellingProductData()}
            renderItem={({item}) => this.renderItem(item)}
            contentContainerStyle={{
                justifyContent: 'center',
                flexDirection: 'row',
                flexWrap: 'wrap',
              }}
            keyExtractor={(item, index)=> '' + index }
            style={{
                flex:1
            }}
        />
    }

}