import _SearchBarComponent from './SearchBarComponent.js'
import _DealGalleryComponent from './DealGalleryComponent.js'
import _CategoryDealsGallery from './CategoryDealsGallery.js'
import _HomeActionsComponent from './HomeActionsComponent.js'
import _ProductCellComponent from './ProductCellComponent.js'
import _ProductGalleryComponent from './ProductGalleryComponent.js'

export const SearchBarComponent = _SearchBarComponent
export const DealGalleryComponent = _DealGalleryComponent
export const CategoryDealsGallery = _CategoryDealsGallery
export const HomeActionsComponent = _HomeActionsComponent
export const ProductCellComponent = _ProductCellComponent
export const ProductGalleryComponent = _ProductGalleryComponent