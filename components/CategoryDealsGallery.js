import React, {Component} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import DealGalleryComponent from './DealGalleryComponent'

export default class CategoryDealsGallery extends Component {

    render() {
        const { title = "", subtitle, data, style = {} } = this.props
        const arrow = <Ionicons name="ios-arrow-forward" size={20} color={'rgb(152, 152, 152)'}/>

        return <View style={style}>
            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                <Text style={{fontWeight:'800', fontSize:20}}>{title}</Text>
                
                <View style={{flexDirection:'row', justifyContent:'center'}}>
                    { subtitle && <Text style={{marginRight:5}}>{subtitle}</Text>}
                    { arrow }
                </View>
                
            </View>
            
            <DealGalleryComponent cellWidth={280} cellSpacing={10} pagingEnabled={false} style={{marginTop:13, height:124}} datas={data} imageKey="img"/>
        </View>

    }

}