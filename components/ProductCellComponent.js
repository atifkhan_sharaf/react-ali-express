import React, {Component} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity
} from 'react-native';

export default class ProductCellComponent extends Component {


    render() {
        const {
            imgHeight=100, 
            product= {}, 
            style = {},
            onSelect
        } = this.props
        
        
        return <TouchableOpacity onPress={()=> {
            debugger
            if (onSelect) {
                onSelect(product)
            }
        }}>
            <View style={[{alignItems:'stretch', padding:10, backgroundColor:'white'}, style]}>
                <Image source={{uri:product.imgLink}} style={{height:imgHeight, resizeMode: Image.resizeMode.contain}}/>

                <Text style ={{marginTop:10, fontSize:13, height:47}}>{ product.title }</Text>

                <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'flex-end', marginTop:10}}>
                    <View>
                        <Text style={{fontSize:12, color:'rgb(178,186,209)'}}>AED {product.oldPrice}</Text>
                        <Text style={{fontWeight:'600', fontSize:18, color:'rgb(84,85,86)'}}>AED {product.price}</Text>
                    </View>

                    <View style={{backgroundColor:'rgb(229,241,220)', padding:5}}>
                        <Text style={{fontSize:10, fontWeight:'600', color:'rgb(93, 170, 51)'}}>15% OFF</Text>
                    </View>
                </View>

            </View>
        </TouchableOpacity>
    }

}