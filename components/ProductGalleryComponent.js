import React, {Component} from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  FlatList,
  Dimensions
} from 'react-native';

import ProductCellComponent from './ProductCellComponent'

import { Ionicons } from '@expo/vector-icons';

export default class ProductGalleryComponent extends Component {
    
    renderItem(item) {
        const {onSelect} = this.props
        return <ProductCellComponent product={item} style={{width:180}} onSelect={onSelect}/>
    }

    renderGallery(){

        const { data } = this.props

        return  <View style={{ height:228, marginTop:5 }} >
            <FlatList
                data={data}
                renderItem={({item}) => this.renderItem(item)}
                keyExtractor={(item, index)=> '' + index }
                horizontal
            />
        </View>
    }

    render() {
        const { title = "", subtitle, style={} } = this.props
        const arrow = <Ionicons name="ios-arrow-forward" size={20} color={'rgb(152, 152, 152)'}/>

        return <View style={style}>
            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                <Text style={{fontWeight:'800', fontSize:20}}>{title}</Text>
                
                <View style={{flexDirection:'row', justifyContent:'center'}}>
                    { subtitle && <Text style={{marginRight:5}}>{subtitle}</Text>}
                    { arrow }
                </View>
                
            </View>
            
            { this.renderGallery()}
        </View>

    }

}