import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';


export default class SearchBarComponent extends Component {

    render() {
      return (
        <View style={styles.searchBarContainer}>
            <TouchableOpacity style={styles.searchButtonHighlight}>
              <Ionicons name="ios-search" size={20} color={'rgb(200, 200, 200)'}/>
            </TouchableOpacity>
            <TextInput
              style={{flex:1}}
              placeholder="I'm shopping for..."
              onChangeText={(text) => this.setState({text})}
            />
        </View>
      );
    }
  
  }
  
  const styles = StyleSheet.create({
    searchBarContainer:{
      borderRadius:5,
      minHeight: 38, 
      backgroundColor:'rgb(244,245,246)',
      alignItems:'stretch',
      justifyContent:'center',
      flexDirection:'row'
    },
    searchButtonHighlight:{
        alignItems:'center', justifyContent:'center', paddingLeft:10, paddingRight:10
    }
  });