import React, {Component} from 'react';
import {
  Image,
  Text,
  View,
  FlatList,
  Dimensions
} from 'react-native';


export default class DealGalleryComponent extends Component {

    cellWidth() {
        const width = Dimensions.get('window').width - 40
        const {cellWidth = width} = this.props
        return cellWidth
    }

    renderItem(item){
        const {cellSpacing = 0, borderRadius=8} = this.props
        const { imageKey } = this.props
        const imgLink = item[imageKey]
        return <Image style={{overflow:'hidden', flex:1, borderRadius, width:this.cellWidth(), marginRight:cellSpacing}} source={{uri: imgLink}}/>
    }

    render() {

        const { datas, key = "key", imageKey, pagingEnabled=true, style = {} } = this.props
        const {borderRadius=8} = this.props

        return <View style={[{ height:200 }, style, {borderRadius,overflow:'hidden'}]} >
            <FlatList
                pagingEnabled={pagingEnabled}
                data={datas}
                renderItem={({item}) => this.renderItem(item)}
                keyExtractor={(item, index)=> '' + index }
                horizontal
            />
        </View>

    }

}
