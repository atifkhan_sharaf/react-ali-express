import React, {Component} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity
} from 'react-native';

function ActionButton({localIconResource, title, style={}}) {
    const imgStyle = {resizeMode: Image.resizeMode.contain, height:25, backgroundColor:'white'}
    const itemStyle = {backgroundColor:'white', flex: 1, justifyContent:'flex-start', alignItems:'center', marginRight:6}
    const textStyle = {marginTop:10, textAlign:'center'}

    return  <TouchableOpacity style={[itemStyle, style]} onPress={()=>{}}>
        <View style={{alignItems:'center'}}>
            <Image source={localIconResource} style={imgStyle}/>
            <Text style={textStyle}>{title}</Text>
        </View>
    </TouchableOpacity>

}

export default class HomeActionsComponent extends Component {

    render() {

        const imgStyle = {resizeMode: Image.resizeMode.contain, height:25}
        const itemStyle = {backgroundColor:'white', flex: 1, justifyContent:'flex-start', alignItems:'center', marginRight:8}
        const textStyle = {marginTop:10}

        return <View style={{alignItems:'stretch', height:70, flexDirection:'row', marginTop:25}}>

            <ActionButton title="Categories" localIconResource={require('../assets/icons/all-categories.png')}/>

            <ActionButton title="Profile" localIconResource={require('../assets/icons/profile.png')}/>

            <ActionButton title="Deals" localIconResource={require('../assets/icons/sale.png')}/>

            <ActionButton title="Track Order" localIconResource={require('../assets/icons/delivery.png')}/>

            <ActionButton title="Cart" style={{marginRight:0}} localIconResource={require('../assets/icons/cart.png')}/>
                                
        </View>
    }

}